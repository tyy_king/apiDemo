package com.m7.restapi.demo;

import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class ApiDemo2 extends TestCase {
//	private static final String account = "N000*****91";//替换为您的账户
//	private static final String secret = "5fdb6********bb88e4b";//替换为您的api密码
	private static final String account = "N00000057149";//替换为您的账户
	private static final String secret = "64fd14a0-8971-11ec-93f7-d18b2fa62474";//替换为您的api密码
	private static final String host = "http://apis.7moor.com";
//	private static Log logger = LogFactory.getLog(ApiDemo.class);
	
	public void test1(){
		log.info("test...........");
//		logger.debug("account = " + account);
	}
	
	public static void main(String[] args) throws IOException {
		String time = getDateTime();
		String sig = md5(account + secret + time);
		//查询坐席状态接口
		String interfacePath = "/v20160818/user/queryUserState/";
		// 客户模板
		interfacePath = "/v20160818/customer/getTemplate/";
		// 外呼接口
		interfacePath = "/v20180426/call/dialout/";
		// 查询获取通话记录和录音
		interfacePath = "/v20180426/cdr/getCCCdr/";
		String url = host + interfacePath + account + "?sig=" + sig;
		System.out.println("url = " + url);
		String auth = base64(account + ":" + time);
		HttpClientBuilder builder = HttpClientBuilder.create();
		CloseableHttpClient client = builder.build();
		HttpPost post = new HttpPost(url);
		post.addHeader("Accept", "application/json");
		post.addHeader("Content-Type","application/json;charset=utf-8");
		post.addHeader("Authorization",auth);
		StringEntity requestEntity = null;
		//根据需要发送的数据做相应替换
        requestEntity = new StringEntity("{\"exten\":\"8000\"}","UTF-8");
        // 外呼接口参数  Local/sip/gateway
        requestEntity = new StringEntity("{\"FromExten\":\"8001\",\"Exten\":\"18782478632\",\"ExtenType\":\"sip\"}","UTF-8");
//        // 查询通话记录接口参数
        requestEntity = new StringEntity("{\"beginTime\":\"2022-03-07 01:00:00\",\"endTime\":\"2022-03-14 01:00:00\"}","UTF-8");
        log.info("requestEntity = {}", IOUtils.toString(requestEntity.getContent()));
		post.setEntity(requestEntity);
		CloseableHttpResponse response = null;
		try {
			response = client.execute(post);
			System.out.println("response = " + response);
			HttpEntity entity = response.getEntity();
			log.info("the response is : " + EntityUtils.toString(entity,"utf8"));
			//外呼接口	[main] INFO com.m7.restapi.demo.ApiDemo - the response is : {"Command":"Response","Succeed":false,"Message":"409 Agent extenType not available","ActionID":"Dialout0.41583064134753256","Response":"Dialout"}
			//查询通话记录接口 {"success":true,"data":[{"_id":"aefb297c-bb03-4a98-a436-c3e2dc40e22f","CALL_SHEET_ID":"aefb297c-bb03-4a98-a436-c3e2dc40e22f","CALL_NO":"02136180739","STATUS":"leak","DISPOSAL_AGENT":"cb9ca920-a012-11ec-86fa-45cc42ee5187","BEGIN_TIME":null,"END_TIME":"2022-03-11 09:22:53","CALLED_NO":"18782478632","OFFERING_TIME":"2022-03-11 09:22:53","CONNECT_TYPE":"dialout","RECORD_FILE_NAME":null,"CUSTOMER_NAME":"未知客户","REF_CALL_SHEET_ID":null,"CALL_ID":"cc-ali-0-1646961753.2954376","ACTION_ID":"Originate0.9198190080425197","PBX":"bj.ali-out.26.8","QUEUE_NAME":"","QUEUE_TIME":null,"FILE_SERVER":"https://pbx-bj-ali26.7moor.com","PROVINCE":"四川省","DISTRICT":"成都市","DISTRICT_CODE":"028","CALL_TIME_LENGTH":0,"EXTEN":"8000","AGENT_NAME":"8000","LABELS":[]},{"_id":"ec972f64-3832-4f26-aa24-470721a7b2b5","CALL_SHEET_ID":"ec972f64-3832-4f26-aa24-470721a7b2b5","CALL_NO":"01080455309","STATUS":"queueLeak","DISPOSAL_AGENT":null,"BEGIN_TIME":null,"END_TIME":"2022-03-10 11:52:47","CALLED_NO":"02136180739","OFFERING_TIME":"2022-03-10 11:52:42","CONNECT_TYPE":"normal","RECORD_FILE_NAME":null,"CUSTOMER_NAME":"未知客户","REF_CALL_SHEET_ID":null,"CALL_ID":"cc-ali-0-1646884362.2951371","ACTION_ID":null,"PBX":"bj.ali-out.26.8","QUEUE_NAME":"坐席","QUEUE_TIME":null,"FILE_SERVER":"https://pbx-bj-ali26.7moor.com","PROVINCE":"北京市","DISTRICT":"北京市","DISTRICT_CODE":"010","CALL_TIME_LENGTH":0,"LABELS":[]},{"_id":"02c77e41-46a2-4a90-8c04-b4bbfec91f19","CALL_SHEET_ID":"02c77e41-46a2-4a90-8c04-b4bbfec91f19","CALL_NO":"02136180739","STATUS":"leak","DISPOSAL_AGENT":"cb9ca920-a012-11ec-86fa-45cc42ee5187","BEGIN_TIME":null,"END_TIME":"2022-03-10 11:52:11","CALLED_NO":"18782478632","OFFERING_TIME":"2022-03-10 11:52:11","CONNECT_TYPE":"dialout","RECORD_FILE_NAME":"monitor/bj.ali-out.26.8/20220310/20220310-115211_N00000057627_21076964_918782478632_cc-ali-0-1646884331.2951367.mp3","CUSTOMER_NAME":"未知客户","REF_CALL_SHEET_ID":null,"CALL_ID":"cc-ali-0-1646884331.2951367","ACTION_ID":null,"PBX":"bj.ali-out.26.8","QUEUE_NAME":"","QUEUE_TIME":null,"FILE_SERVER":"https://pbx-bj-ali26.7moor.com","PROVINCE":"四川省","DISTRICT":"成都市","DISTRICT_CODE":"028","CALL_TIME_LENGTH":0,"EXTEN":"8000","AGENT_NAME":"8000","LABELS":[]},{"_id":"c37a8013-ac95-404a-a50f-5dba2ce8d675","CALL_SHEET_ID":"c37a8013-ac95-404a-a50f-5dba2ce8d675","CALL_NO":"02136180739","STATUS":"notDeal","DISPOSAL_AGENT":"cb9ca920-a012-11ec-86fa-45cc42ee5187","BEGIN_TIME":null,"END_TIME":"2022-03-10 11:51:50","CALLED_NO":"18782478632","OFFERING_TIME":"2022-03-10 11:51:45","CONNECT_TYPE":"dialout","RECORD_FILE_NAME":"monitor/bj.ali-out.26.8/20220310/20220310-115145_N00000057627_21076964_918782478632_cc-ali-0-1646884305.2951365.mp3","CUSTOMER_NAME":"未知客户","REF_CALL_SHEET_ID":null,"CALL_ID":"cc-ali-0-1646884305.2951365","ACTION_ID":null,"PBX":"bj.ali-out.26.8","QUEUE_NAME":"","QUEUE_TIME":null,"FILE_SERVER":"https://pbx-bj-ali26.7moor.com","PROVINCE":"四川省","DISTRICT":"成都市","DISTRICT_CODE":"028","HANGUP_USER":"agent","CALL_TIME_LENGTH":0,"EXTEN":"8000","AGENT_NAME":"8000","LABELS":[]}],"count":4}
			// 	{"success":false,"message":"beginTime and endTime must within a week!"}
			// 录音地址： https://pbx-bj-ali26.7moor.com/monitor/bj.ali-out.26.8/20220313/20220313-163245_N00000057149__913210471093_cc-ali-0-1647160364.2965042.mp3

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null){
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static String md5 (String text) {
		return DigestUtils.md5Hex(text).toUpperCase();
	}
	public static String base64 (String text) {
		byte[] b = text.getBytes();
		Base64 base64 = new Base64();
		b = base64.encode(b);
		String s = new String(b);
		return s;
	}
	public static String getDateTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(new Date());
	}
}
